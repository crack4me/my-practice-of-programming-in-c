#include <iostream>

using namespace std;

int binary_exponentiation_first(int a, int b) //Mr.Obviousness
{
    if (b == 0)
        return 1;
    if (b % 2 == 1)
        return binary_exponentiation_first(a,b-1) * a;
    else
        {
        int c = binary_exponentiation_first(a,b/2);
        return c * c;
        }
}

int binary_exponentiation_without_recursion(int a, int b)
{
    int result = 1;
    while (b)
        if (b % 2 != 0)
            {
                result *= a;
                b--;
            }
        else
            {
                a *= a;
                b /= 2;
            }
      return result;
}

int binary_exponentiation_opt(int a, int b) //without recursion
{
    unsigned long int result = 1;
    while (b)
    {
        if (b & 1) //isn't even?
            result *= a;
        a *= a;
        b >>= 1; //b /= 2;
    }
    return result;
}

int the_Fibonacci_number(int number) //easily
{
  return (number==0)?0:(number<=2)?1:(the_Fibonacci_number(number-1)+the_Fibonacci_number(number-2));
}

void Fibonacci_series(int count)
{
    int i=0;
    while (i < count)
    {
        cout << the_Fibonacci_number(i) << ' ';
        i++;
    }
}


/*int the_Fibonacci_number_matrix(int number)
{
    while number>64
    {
        //-----
    }
} */

int main()
{
    int a,b,n;
    cin >> a >> b >> n;
    cout << binary_exponentiation_opt(a,b) << endl;
    Fibonacci_series(n);
    return 0;
}
